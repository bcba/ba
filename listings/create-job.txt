>kubectl run t-1 --image=docker-registry.default.svc:5000/rim-tu/jmeter:2.13 \
--restart=OnFailure
job.batch "t-1" created

>oc run t-2 --image=docker-registry.default.svc:5000/rim-tu/jmeter:2.13 \
--restart=OnFailure
pod "t-2" created

>oc get jobs
NAME      DESIRED   SUCCESSFUL   AGE
t-1       1         1            1m