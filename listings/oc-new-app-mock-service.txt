D:\Documents\Abs2019\soapui\configMaps>oc new-app --name=lup-mock-soap-bcm --docker-image=docker-registry.default.svc:5000/rim-tu/soapui:5.4 --env-file=bcm-cm.txt
--> Found Docker image 66f53b9 (About an hour old) from docker-registry.default.svc:5000 for "docker-registry.default.svc:5000/rim-tu/soapui:5.4"

    * An image stream will be created as "lup-mock-soap-bcm:5.4" that will track this image
    * This image will be deployed in deployment config "lup-mock-soap-bcm"
    * Port 8080/tcp will be load balanced by service "lup-mock-soap-bcm"
      * Other containers can access this service through the hostname "lup-mock-soap-bcm"

--> Creating resources ...
    imagestream "lup-mock-soap-bcm" created
    deploymentconfig "lup-mock-soap-bcm" created
    service "lup-mock-soap-bcm" created
--> Success
    Application is not exposed. You can expose services to the outside world by executing one or more of the commands below:
     'oc expose svc/lup-mock-soap-bcm'
    Run 'oc status' to view your app.

D:\Documents\Abs2019\soapui\configMaps>oc expose svc/lup-mock-soap-bcm --hostname=bcm-mock-service.inet-abnahme.ose.db.de --name=mock-bcm
route "mock-bcm" exposed

D:\Documents\Abs2019\soapui\configMaps>oc edit dc lup-mock-soap-bcm
deploymentconfig "lup-mock-soap-bcm" edited

D:\Documents\Abs2019\soapui\configMaps>oc get dc lup-mock-soap-bcm -o yaml
apiVersion: v1
kind: DeploymentConfig
metadata:
  annotations:
    openshift.io/generated-by: OpenShiftNewApp
  creationTimestamp: 2019-05-15T05:51:19Z
  generation: 3
  labels:
    app: lup-mock-soap-bcm
  name: lup-mock-soap-bcm
  namespace: rim-tu
  resourceVersion: "258248146"
  selfLink: /oapi/v1/namespaces/rim-tu/deploymentconfigs/lup-mock-soap-bcm
  uid: 76353c6c-76d5-11e9-b172-0a53e3a703b3
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    app: lup-mock-soap-bcm
    deploymentconfig: lup-mock-soap-bcm
  strategy:
    activeDeadlineSeconds: 21600
    resources: {}
    rollingParams:
      intervalSeconds: 1
      maxSurge: 25%
      maxUnavailable: 25%
      timeoutSeconds: 600
      updatePeriodSeconds: 1
    type: Rolling
  template:
    metadata:
      annotations:
        openshift.io/generated-by: OpenShiftNewApp
      creationTimestamp: null
      labels:
        app: lup-mock-soap-bcm
        deploymentconfig: lup-mock-soap-bcm
    spec:
      containers:
      - env:
        - name: MOCK
          value: bcm
        - name: PROJECTFILE
          value: DB.RIM_soapui-project_with_5min_delay.xml
        - name: SOAPMOCKNAME
          value: BCMMockService
        image: docker-registry.default.svc:5000/rim-tu/soapui@sha256:1f7e24aa847165b43910f8e17144be9a5e0aae40ccbbc4c128ce6d0708b9cb5c
        imagePullPolicy: IfNotPresent
        name: lup-mock-soap-bcm
        ports:
        - containerPort: 8080
          protocol: TCP
        resources:
          limits:
            memory: 1536Mi
          requests:
            memory: 256Mi
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
  test: false
  triggers:
  - type: ConfigChange
  - imageChangeParams:
      automatic: true
      containerNames:
      - lup-mock-soap-bcm
      from:
        kind: ImageStreamTag
        name: lup-mock-soap-bcm:5.4
        namespace: rim-tu
      lastTriggeredImage: docker-registry.default.svc:5000/rim-tu/soapui@sha256:1f7e24aa847165b43910f8e17144be9a5e0aae40ccbbc4c128ce6d0708b9cb5c
    type: ImageChange
status:
  availableReplicas: 1
  conditions:
  - lastTransitionTime: 2019-05-15T05:51:53Z
    lastUpdateTime: 2019-05-15T05:51:53Z
    message: Deployment config has minimum availability.
    status: "True"
    type: Available
  - lastTransitionTime: 2019-05-15T06:07:41Z
    lastUpdateTime: 2019-05-15T06:07:43Z
    message: replication controller "lup-mock-soap-bcm-2" successfully rolled out
    reason: NewReplicationControllerAvailable
    status: "True"
    type: Progressing
  details:
    causes:
    - type: ConfigChange
    message: config change
  latestVersion: 2
  observedGeneration: 3
  readyReplicas: 1
  replicas: 1
  unavailableReplicas: 0
  updatedReplicas: 1

D:\Documents\Abs2019\soapui\configMaps>