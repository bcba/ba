Started by user Juraj Balasko (jurajbalasko)
Obtained trigger-lup-mocks-deployment-openshift.groovy from git ssh://git@rimentwtools-bitbucket.st1-test.comp.db.de:7999/rt/jenkins-tools.git
Running in Durability level: MAX_SURVIVABILITY
Loading library imperative-when-library@master
Attempting to resolve master from remote references...
 > git --version # timeout=10
 > git ls-remote -h ssh://git@rimentwtools-bitbucket.st1-test.comp.db.de:7999/rt/imperative-when-library.git # timeout=10
Found match: refs/heads/master revision 478ebb36d34d3607c92aab2dd0e580d8c3275ee2
No credentials specified
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url ssh://git@rimentwtools-bitbucket.st1-test.comp.db.de:7999/rt/imperative-when-library.git # timeout=10
Fetching without tags
Fetching upstream changes from ssh://git@rimentwtools-bitbucket.st1-test.comp.db.de:7999/rt/imperative-when-library.git
 > git --version # timeout=10
 > git fetch --no-tags --progress ssh://git@rimentwtools-bitbucket.st1-test.comp.db.de:7999/rt/imperative-when-library.git +refs/heads/*:refs/remotes/origin/*
Checking out Revision 478ebb36d34d3607c92aab2dd0e580d8c3275ee2 (master)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 478ebb36d34d3607c92aab2dd0e580d8c3275ee2
Commit message: "COPS-184"
 > git rev-list --no-walk 478ebb36d34d3607c92aab2dd0e580d8c3275ee2 # timeout=10
[Pipeline] Start of Pipeline

[Pipeline] node

Running on Jenkins in /jenkins/.jenkins/workspace/trigger-lup-mocks-deployment-openshift
[Pipeline] {

[Pipeline] stage

[Pipeline] { (Declarative: Checkout SCM)

[Pipeline] checkout (show)
[Pipeline] }
[Pipeline] // stage
[Pipeline] withEnv

[Pipeline] {

[Pipeline] stage

[Pipeline] { (Deployment)

[Pipeline] parallel

[Pipeline] { (Branch: Deploy BCM-Mock)

[Pipeline] { (Branch: Deploy PlanET-Mock)

[Pipeline] { (Branch: Deploy Edith-Mock)

[Pipeline] { (Branch: Deploy KRWD-Mock)

[Pipeline] stage

[Pipeline] { (Deploy BCM-Mock)

[Pipeline] stage

[Pipeline] { (Deploy PlanET-Mock)

[Pipeline] stage

[Pipeline] { (Deploy Edith-Mock)

[Pipeline] stage

[Pipeline] { (Deploy KRWD-Mock)

[Pipeline] echo

Deploying bcm-Mock
[Pipeline] withEnv

[Pipeline] {

[Pipeline] echo

Deploying planet-Mock
[Pipeline] withEnv

[Pipeline] {

[Pipeline] echo

Deploying edith-Mock
[Pipeline] withEnv

[Pipeline] {

[Pipeline] echo

Deploying krwd-Mock
[Pipeline] withEnv

[Pipeline] {

[Pipeline] openshiftDeploy

NOTE: steps like this one from the OpenShift Pipeline Plugin will not be supported against OpenShift API Servers later than v3.11


Starting "Trigger OpenShift Deployment" with deployment config "rim-lup-mock-soap-bcm" from the project "rim-tu".
Operation will timeout after 600000 milliseconds
[Pipeline] openshiftDeploy

NOTE: steps like this one from the OpenShift Pipeline Plugin will not be supported against OpenShift API Servers later than v3.11


Starting "Trigger OpenShift Deployment" with deployment config "rim-lup-mock-soap-planet" from the project "rim-tu".
Operation will timeout after 600000 milliseconds
[Pipeline] openshiftDeploy

NOTE: steps like this one from the OpenShift Pipeline Plugin will not be supported against OpenShift API Servers later than v3.11


Starting "Trigger OpenShift Deployment" with deployment config "rim-lup-mock-soap-edith" from the project "rim-tu".
Operation will timeout after 600000 milliseconds
[Pipeline] openshiftDeploy

NOTE: steps like this one from the OpenShift Pipeline Plugin will not be supported against OpenShift API Servers later than v3.11


Starting "Trigger OpenShift Deployment" with deployment config "rim-lup-mock-soap-krwd" from the project "rim-tu".
Operation will timeout after 600000 milliseconds


Exiting "Trigger OpenShift Deployment" successfully; deployment "rim-lup-mock-soap-edith-28" has completed with status:  [Complete].


Exiting "Trigger OpenShift Deployment" successfully; deployment "rim-lup-mock-soap-bcm-44" has completed with status:  [Complete].
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] }
[Pipeline] // stage
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] }
[Pipeline] // stage
[Pipeline] }


Exiting "Trigger OpenShift Deployment" successfully; deployment "rim-lup-mock-soap-krwd-39" has completed with status:  [Complete].


Exiting "Trigger OpenShift Deployment" successfully; deployment "rim-lup-mock-soap-planet-35" has completed with status:  [Complete].
[Pipeline] }
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] }
[Pipeline] // stage
[Pipeline] // stage
[Pipeline] }
[Pipeline] }
[Pipeline] // parallel
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS