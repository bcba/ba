pipeline {
    agent any

    parameters {
        booleanParam( name: 'BCM_Deployment', defaultValue: false, description: 'Deploy BCM-Mock?')
        booleanParam( name: 'PLANET_Deployment', defaultValue: false, description: 'Deploy PLANET-Mock?')
        booleanParam( name: 'EDITH_Deployment', defaultValue: false, description: 'Deploy EDITH-Mock?')
        booleanParam( name: 'KRWD_Deployment', defaultValue: false, description: 'Deploy KRWD-Mock?')
    }

    stages {
        stage ("Deployment"){
            parallel {
                stage ("Deploy BCM-Mock"){
                    when { environment name: 'BCM_Deployment', value: 'true' }
                    steps {
                        deploy('bcm')
                    }
                }
                stage ("Deploy PlanET-Mock"){
                    when { environment name: 'PLANET_Deployment', value: 'true' }
                    steps {
                        deploy('planet')
                    }
                }
                stage ("Deploy Edith-Mock"){
                    when { environment name: 'EDITH_Deployment', value: 'true' }
                    steps {
                        deploy('edith')
                    }
                }
                stage ("Deploy KRWD-Mock"){
                    when { environment name: 'KRWD_Deployment', value: 'true' }
                    steps {
                        deploy('krwd')
                    }
                }
            }
        }
    }
}

def deploy(String mock) {
    echo "Deploying ${mock}-Mock"
    withEnv([
        'KUBERNETES_SERVICE_HOST=https://console.inet-abnahme.ose.db.de',
        'AUTH_TOKEN=',
        'SKIP_TLS=true',
        'HTTP_PROXY=localhost:8888',
        'HTTPS_PROXY=localhost:8888'
    ]) {
        openshiftDeploy(
                namespace: 'rim-tu',
                depCfg: "rim-lup-mock-soap-" + mock
        )
    }
}